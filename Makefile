all:
	javac -cp build/ -d build -sourcepath build/ src/lab/*.java

run: 
	java -cp build/ lab/Lab3
jar:
	jar cfm lab3.jar manifest -C build/ .

clean:
	rm build/lab/*.class
