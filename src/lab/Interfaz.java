
package lab;

import nrc.fuzzy.*;
import javax.swing.event.*;  
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.util.Hashtable;
import java.util.logging.Logger;
import java.util.ArrayList;

public class Interfaz extends javax.swing.JFrame {

    // almacena los valores obtenidos de los barras deslizantes
    private double valor_1;
    private double valor_2;
    private double valor_3;
    private double valor_4;
    private double valor_5;

    // el max de una barra y el minino
    int slider_max = 50;
    int slider_min = 0;
    // contiene el icono de la pregunta
    ImageIcon icono_pregunta;

    // hashtable para ingresar los numeros que se mostraran en las barras deslizantes
    java.util.Hashtable<Integer,JLabel> labelTable = new java.util.Hashtable<Integer,JLabel>();  

    private Logica logica;


    private javax.swing.JButton boton_pregunta1;
    private javax.swing.JButton boton_pregunta2;
    private javax.swing.JButton boton_pregunta3;
    private javax.swing.JButton boton_pregunta4;
    private javax.swing.JButton boton_pregunta5;
    private javax.swing.JButton boton_resultado;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panel_preguntas;
    private javax.swing.JPanel panel_ventana;
    private javax.swing.JScrollPane scroll_preguntas;
    private javax.swing.JScrollPane scroll_ventana;
    private javax.swing.JSlider slider_1;
    private javax.swing.JSlider slider_2;
    private javax.swing.JSlider slider_3;
    private javax.swing.JSlider slider_4;
    private javax.swing.JSlider slider_5;
    private javax.swing.JTextArea texto_mensaje;

 
    /***********
     * Constructor
     ***********/
    public Interfaz(){

        logica = new Logica();

        // cargamos la imagen de la pregunta
        ImageIcon icon = new ImageIcon("otros/pregunta.jpg");
        // del icono se obtiene un objeto imagen
        Image img = icon.getImage();
        // se redimensiona la imagen, en una imagen de 30x30
        Image newimg = img.getScaledInstance( 30, 30,  java.awt.Image.SCALE_SMOOTH );
        // se pasa a objeto 'imagenIcon' otra vez, pero mas chico
        this.icono_pregunta = new ImageIcon (newimg);

        // crea la tabla hash para despues agregarlo como etique para las barras deslizantes
        crear_tabla();

        // inicia la interfaz
        iniciar();

        // agrega el hashtable a las barras deslizantes
        agregar_tabla();

        // agrega la imagen de los iconos a los botones
        agregar_iconos();
    }

    /***********
     * Crea la table hash, para agregarla como etiqueta a las barras deslizantes
     * El minimo y el maximo esta multiplicado x10 para despues utilizar valores
     * decimales, dividiendo lo por 10 al valor obtenido de la barra deslizante
     ***********/
    private void crear_tabla(){

        // una tabla para despues agregarla al slider, para poder poner etiquetas con 'valores' decimales
        // la idea es hacer eso y despues se divide, en esta caso por 10, para obtener decimales
        double valor;

        for(int i = slider_min; i <= slider_max; i++){

            // cada 5 valores pone la etiqueta en hashtable
            if( (i%5) == 0  ){
            
                // divide el entero por 10 y se pasa a 'double'
                valor = i / 10.0;

                // asocia el primer argumentos con el string del siguiente argumento de 'put'
                // las que se agregen son las que se mostraran en la barra deslizante
                this.labelTable.put( new Integer( i ), new JLabel(""+valor ) );

            }
        }
    }

    
    /***********
     * Agrega el icono de la pregunta a cada boton de explicacion
     ***********/
    private void agregar_iconos(){

        boton_pregunta1.setIcon(icono_pregunta);
        boton_pregunta2.setIcon(icono_pregunta);
        boton_pregunta3.setIcon(icono_pregunta);
        boton_pregunta4.setIcon(icono_pregunta);
        boton_pregunta5.setIcon(icono_pregunta);
    }

    /***********
     * Agrega el hashtable creado a cada barra deslizadora 
     * para que muestre los valores divididos en 10 
     ***********/
    private void agregar_tabla(){

        slider_1.setLabelTable(labelTable);
        slider_2.setLabelTable(labelTable);
        slider_3.setLabelTable(labelTable);
        slider_4.setLabelTable(labelTable);
        slider_5.setLabelTable(labelTable);

    }
    
    /***********
     * obtiene un arrayList de los valores obtenidos por las barras deslizadoras
     * @return ArrayList - un arrayList de 'double' con los valores de la barra deslizadora
     ***********/
    public ArrayList<Double> getValores(){

        ArrayList<Double> array = new ArrayList<Double>(12);

        array.add(valor_1);
        array.add(valor_2);
        array.add(valor_3);
        array.add(valor_4);
        array.add(valor_5);

        return array;
    }

    /***********
     * Obtiene los valores de las barras deslizantes y los agrega las variables 'double' de la
     * clase, para despues pedirlos. Se hace la divicion por 10 para obtener valores decimales
     ***********/
    private void setValores(){

        valor_1 = slider_1.getValue() / 10.0;
        valor_2 = slider_2.getValue() / 10.0;
        valor_3 = slider_3.getValue() / 10.0;
        valor_4 = slider_4.getValue() / 10.0;
        valor_5 = slider_5.getValue() / 10.0;
    }

    
    /***********
     * Agrega el String 'mensaje' en el cuadro de texto de la interfaz
     * @param mensaje - String con el mensaje que se mostrara
     ***********/
    public void mostrar(String mensaje){

        texto_mensaje.setText(mensaje);
    }

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interfaz().setVisible(true);
            }
        });
    }


    /****************
     * Botones
     ****************/
    private void boton_pregunta1MouseClicked(java.awt.event.MouseEvent evt) {                                             

        // al hacer click en el boton saldra un mensaje de dialogo con una descripcion de la pregunta
        JOptionPane.showMessageDialog(null, "Sensación de que los latidos cardiacos aumentan con violencia.\n"
                                           +"Indicar el nivel de sensación deslizando la barra entre 0(sin sensación)\n" +
                                            "o 5 (sensación muy fuerte) ");
    }                                            

    private void boton_pregunta2MouseClicked(java.awt.event.MouseEvent evt) {                                             

        JOptionPane.showMessageDialog(null, "Cuando esta presenta la falta de aire o sensación de ahogo.\n"+
                                            "Debe indicar entre 0 y 5 que tan fuerte o recurrente es la sensación,siendo\n"+
                                            "5 una sensación muy fuerte");
    }                                            

    private void boton_pregunta3MouseClicked(java.awt.event.MouseEvent evt) {                                             

        JOptionPane.showMessageDialog(null, "Cuando hay una sensación de opresión o malestar torácico.\n"+
                                            "Con un valor 0 cuando no hay malestar y 5 cuando el malestar"+
                                            "es alto");
    }                                            

    private void boton_pregunta4MouseClicked(java.awt.event.MouseEvent evt) {                                             

        JOptionPane.showMessageDialog(null, "Miedo a perder el control o a volverse loco");
    }                                            

    private void boton_pregunta5MouseClicked(java.awt.event.MouseEvent evt) {                                             

        JOptionPane.showMessageDialog(null, "Mide la sensación de escalofríos o cuando se tienen sofocaciones.\n"+
                                            "Cambios en la sensación de la temperatura, calor o frío. Siendo un valor\n"+
                                            "5 cuando la sensación es muy alta");
    }                                            


    /**************
     * Al hacer click en el boton de consulta, en la interfaz saldra el resultado del test.
     * Se toman los valores de las barras deslizadoras y luego son pasados al metodo de la clase
     * Logica para hacer el calculo con Logica Difusa
     *
     **************/
    private void boton_resultadoMouseClicked(java.awt.event.MouseEvent evt) {                                             

        setValores();
        try{

            logica.setValores(getValores());
        }
        catch(FuzzyException e){
            System.out.println("error: ");
            e.printStackTrace();
        }

        mostrar(logica.resultadoS);

    }    



    /**************
     * Se inicializa los componentes de la interfaz
     **************/
    private void iniciar(){

        scroll_ventana = new javax.swing.JScrollPane();
        panel_ventana = new javax.swing.JPanel();
        scroll_preguntas = new javax.swing.JScrollPane();
        panel_preguntas = new javax.swing.JPanel();
        slider_1 = new javax.swing.JSlider();
        boton_pregunta1 = new javax.swing.JButton();
        slider_2 = new javax.swing.JSlider();
        slider_3 = new javax.swing.JSlider();
        slider_4 = new javax.swing.JSlider();
        slider_5 = new javax.swing.JSlider();
        boton_pregunta2 = new javax.swing.JButton();
        boton_pregunta3 = new javax.swing.JButton();
        boton_pregunta4 = new javax.swing.JButton();
        boton_pregunta5 = new javax.swing.JButton();
        boton_resultado = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        texto_mensaje = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panel_ventana.setBackground(new java.awt.Color(205, 111, 111));

        panel_preguntas.setBackground(new java.awt.Color(214, 230, 217));

        slider_1.setBackground(new java.awt.Color(214, 230, 217));
        slider_1.setMajorTickSpacing(5);
        slider_1.setMaximum(slider_max);
        slider_1.setMinorTickSpacing(1);
        slider_1.setPaintLabels(true);
        slider_1.setPaintTicks(true);
        slider_1.setValue(0);
        slider_1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(1, 1, 1), new java.awt.Color(1, 1, 1)), "1.Palpitaciones", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 18))); // NOI18N
        slider_1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        boton_pregunta1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                boton_pregunta1MouseClicked(evt);
            }
        });

        slider_2.setBackground(new java.awt.Color(214, 230, 217));
        slider_2.setMajorTickSpacing(5);
        slider_2.setMaximum(slider_max);
        slider_2.setMinorTickSpacing(1);
        slider_2.setPaintLabels(true);
        slider_2.setPaintTicks(true);
        slider_2.setValue(0);
        slider_2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(1, 1, 1), new java.awt.Color(1, 1, 1)), "2.Falta de aire", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 18))); // NOI18N
        slider_2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        slider_3.setBackground(new java.awt.Color(214, 230, 217));
        slider_3.setMajorTickSpacing(5);
        slider_3.setMaximum(slider_max);
        slider_3.setMinorTickSpacing(1);
        slider_3.setPaintLabels(true);
        slider_3.setPaintTicks(true);
        slider_3.setValue(0);
        slider_3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(1, 1, 1), new java.awt.Color(1, 1, 1)), "3.Molestares torácicos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 18))); // NOI18N
        slider_3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        slider_4.setBackground(new java.awt.Color(214, 230, 217));
        slider_4.setMajorTickSpacing(5);
        slider_4.setMaximum(slider_max);
        slider_4.setMinorTickSpacing(1);
        slider_4.setPaintLabels(true);
        slider_4.setPaintTicks(true);
        slider_4.setValue(0);
        slider_4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(1, 1, 1), new java.awt.Color(1, 1, 1)), "4.Miedo a la locura", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 18))); // NOI18N
        slider_4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        slider_5.setBackground(new java.awt.Color(214, 230, 217));
        slider_5.setMajorTickSpacing(5);
        slider_5.setMaximum(slider_max);
        slider_5.setMinorTickSpacing(1);
        slider_5.setPaintLabels(true);
        slider_5.setPaintTicks(true);
        slider_5.setValue(0);
        slider_5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(1, 1, 1), new java.awt.Color(1, 1, 1)), "5.Escalofrios", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 18))); // NOI18N
        slider_5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));



        boton_pregunta2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                boton_pregunta2MouseClicked(evt);
            }
        });

        boton_pregunta3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                boton_pregunta3MouseClicked(evt);
            }
        });

        boton_pregunta4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                boton_pregunta4MouseClicked(evt);
            }
        });

        boton_pregunta5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                boton_pregunta5MouseClicked(evt);
            }
        });



        javax.swing.GroupLayout panel_preguntasLayout = new javax.swing.GroupLayout(panel_preguntas);
        panel_preguntas.setLayout(panel_preguntasLayout);
        panel_preguntasLayout.setHorizontalGroup(
            panel_preguntasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_preguntasLayout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(panel_preguntasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addComponent(slider_5, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(boton_pregunta5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addComponent(slider_4, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(boton_pregunta4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addComponent(slider_3, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(boton_pregunta3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addComponent(slider_2, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(boton_pregunta2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addComponent(slider_1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(boton_pregunta1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        panel_preguntasLayout.setVerticalGroup(
            panel_preguntasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_preguntasLayout.createSequentialGroup()
                .addGroup(panel_preguntasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(slider_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(boton_pregunta1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(panel_preguntasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(slider_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(boton_pregunta2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(panel_preguntasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(slider_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(boton_pregunta3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(panel_preguntasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(slider_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(boton_pregunta4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(panel_preguntasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(slider_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_preguntasLayout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(boton_pregunta5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        slider_1.getAccessibleContext().setAccessibleName("Pregunta");

        scroll_preguntas.setViewportView(panel_preguntas);

        boton_resultado.setText("Consultar");
        boton_resultado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                boton_resultadoMouseClicked(evt);
            }
        });

        texto_mensaje.setColumns(20);
        texto_mensaje.setRows(5);
        jScrollPane1.setViewportView(texto_mensaje);

        javax.swing.GroupLayout panel_ventanaLayout = new javax.swing.GroupLayout(panel_ventana);
        panel_ventana.setLayout(panel_ventanaLayout);
        panel_ventanaLayout.setHorizontalGroup(
            panel_ventanaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_ventanaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_ventanaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scroll_preguntas, javax.swing.GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE)
                    .addGroup(panel_ventanaLayout.createSequentialGroup()
                        .addComponent(boton_resultado)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1)))
                .addContainerGap())
        );
        panel_ventanaLayout.setVerticalGroup(
            panel_ventanaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_ventanaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scroll_preguntas, javax.swing.GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addGroup(panel_ventanaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_ventanaLayout.createSequentialGroup()
                        .addComponent(boton_resultado)
                        .addGap(52, 52, 52))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_ventanaLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        scroll_ventana.setViewportView(panel_ventana);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scroll_ventana)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scroll_ventana)
        );

        pack();

    }


}
