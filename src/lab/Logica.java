/************
 *
 * Clase para realizar el trabajo logico, usando Fuzzy Logic
 *
 ************/
package lab;

import nrc.fuzzy.*;
import java.util.ArrayList;


public class Logica{

    // arreglo para las preguntas
    FuzzyVariable preg[]; 

    // FuzzyVarieble para almacenar el resultado
    FuzzyVariable resultado; 

    // arreglo de reglas
    FuzzyRule reglas[];

    // resultado de la inferencia logica
    public String resultadoS;


    /******
     *  Contructor
     */
    public Logica(){

        // crea arreglo de fuzzyVariable para almacenar las preguntas
        preg = new FuzzyVariable[5];

        // crea el arreglo de las 32 reglas
        reglas = new FuzzyRule[32];

        // se crean la FuzzyVariables, con sus condiciones
        iniciarVariables();
        // inicializa las 32 reglas y las define
        iniciar_reglas();
    }

    /***************
     * Recibe los valores leidos de la interfaz 
     * y hace la inferencia de las reglas
     *****************/
    public void setValores(ArrayList<Double> array) throws FuzzyException{

        
        FuzzyValue valor[] = new FuzzyValue[5];

        // tomas los valores de la interfaz y crea 5 FuzzyValue con ese numero y
        //  una simple representacion grafica de ese valor
        for ( int i = 0; i < 5; i++){

            valor[i] = new FuzzyValue(preg[i],new SingletonFuzzySet(array.get(i)));
        }
        

        for ( int i = 0; i < 32; i++){

            // elimina entradas anteriormente ingresadas
            reglas[i].removeAllInputs();

            // agrega las 5 fuzzyValue creadas a las 32 reglas
            for( int j = 0; j < 5; j++){

                // agrega un valor a la regla
                reglas[i].addInput(valor[j]);
            }

        }

        // comprueba las entrada con las reglas y calcula el resultado
        math_ruler();

        
    }

    /************
     * comprueba las reglas con los valores ingresados y 
     * calcula el resultado 
     ************/
    public void math_ruler(){
        
        // valor al cual se le unen los valores resultantes de las reglas
        FuzzyValue valor_resultado = null;
        FuzzyValueVector estadoVector ;
        double resultado;


        try{

            // para las 32 reglas comprueba si los valores ingresados concuerdan
            // con la definicion de estas reglas
            for( int i = 0; i < 32 ; i++){

                // si los imputs concuercan con la regla
                if( reglas[i].testRuleMatching()){

                    // obtenemos el vector con el resultado, pares de datos, el grado de posibilidad (eje y)
                    // y un valor nitido (eje x)
                    estadoVector = reglas[i].execute();
                    

                    // si esta vacio => la primera vez hay un match de un regla
                    if(valor_resultado == null){

                        // obtenemos fuzzyValue del fuzzyValueVector
                        valor_resultado = estadoVector.fuzzyValueAt(0);
                    }
                    else{

                        // unimos el FuzzyValue actual con el obtenido del fuzzyValueVector
                        valor_resultado.fuzzyUnion(estadoVector.fuzzyValueAt(0));
                    }

                }
            }

            // se obtiene un valor nítido de la defuzificación
            resultado = valor_resultado.momentDefuzzify();


            // si el resultado corresponde a "no"
            if(  resultado >= 0 && resultado < 1.5 && valor_resultado.fuzzyMatch("no")){

                resultadoS = "Su sintomatología no califica como ataque de pánico.";

            }
            // si el resultado corresponde a "mederado"
            else if( resultado >= 1.5 && resultado <= 4.0 && valor_resultado.fuzzyMatch("moderado")){

                resultadoS = "Sus síntomas indican que podría estar sufriendo un ataque de pánico.";
            }
            // si el resultado corresponde a "alto"
            else if(resultado > 4.0 && valor_resultado.fuzzyMatch("alto")){

                resultadoS = "Definitivamente usted está experimentando un ataque de pánico.";
                
            }

        }
        catch(FuzzyException e){
            System.out.println("error 'math_ruler':");
            e.printStackTrace();
        }

    }



    /************
     *
     * Crea las reglas del problema
     * Se crean un total de 32 reglas
     *
     *************/
    public void iniciar_reglas(){

        try{


            // cuando todas las preguntas dan como resultado 'nada' no se tiene posibilidades de crisis de panico
            reglas[0] = new FuzzyRule();

            reglas[0].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[0].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[0].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[0].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[0].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[0].addConclusion(new FuzzyValue(resultado, "no")); 




            // cuando todos los resultados a las preguntas son altos entonces se tiene crisis de panico
            reglas[1] = new FuzzyRule();

            reglas[1].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[1].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[1].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[1].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[1].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[1].addConclusion(new FuzzyValue(resultado, "alto")); 





            reglas[2] = new FuzzyRule();

            reglas[2].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[2].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[2].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[2].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[2].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[2].addConclusion(new FuzzyValue(resultado, "alto")); 


            reglas[3] = new FuzzyRule();

            reglas[3].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[3].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[3].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[3].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[3].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[3].addConclusion(new FuzzyValue(resultado, "moderado")); 



            reglas[4] = new FuzzyRule();

            reglas[4].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[4].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[4].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[4].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[4].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[4].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[5] = new FuzzyRule();

            reglas[5].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[5].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[5].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[5].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[5].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[5].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[6] = new FuzzyRule();

            reglas[6].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[6].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[6].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[6].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[6].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[6].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[7] = new FuzzyRule();

            reglas[7].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[7].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[7].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[7].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[7].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[7].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[8] = new FuzzyRule();

            reglas[8].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[8].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[8].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[8].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[8].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[8].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[9] = new FuzzyRule();

            reglas[9].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[9].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[9].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[9].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[9].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[9].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[10] = new FuzzyRule();

            reglas[10].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[10].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[10].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[10].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[10].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[10].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[11] = new FuzzyRule();

            reglas[11].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[11].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[11].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[11].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[11].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[11].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[12] = new FuzzyRule();

            reglas[12].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[12].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[12].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[12].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[12].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[12].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[13] = new FuzzyRule();

            reglas[13].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[13].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[13].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[13].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[13].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[13].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[14] = new FuzzyRule();

            reglas[14].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[14].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[14].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[14].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[14].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[14].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[15] = new FuzzyRule();

            reglas[15].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[15].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[15].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[15].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[15].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[15].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[16] = new FuzzyRule();

            reglas[16].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[16].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[16].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[16].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[16].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[16].addConclusion(new FuzzyValue(resultado, "moderado")); 




            reglas[17] = new FuzzyRule();

            reglas[17].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[17].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[17].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[17].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[17].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[17].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[18] = new FuzzyRule();

            reglas[18].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[18].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[18].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[18].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[18].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[18].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[19] = new FuzzyRule();

            reglas[19].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[19].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[19].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[19].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[19].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[19].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[20] = new FuzzyRule();

            reglas[20].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[20].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[20].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[20].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[20].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[20].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[21] = new FuzzyRule();

            reglas[21].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[21].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[21].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[21].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[21].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[21].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[22] = new FuzzyRule();

            reglas[22].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[22].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[22].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[22].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[22].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[22].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[23] = new FuzzyRule();

            reglas[23].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[23].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[23].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[23].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[23].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[23].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[24] = new FuzzyRule();

            reglas[24].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[24].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[24].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[24].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[24].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[24].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[25] = new FuzzyRule();

            reglas[25].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[25].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[25].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[25].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[25].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[25].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[26] = new FuzzyRule();

            reglas[26].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[26].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[26].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[26].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[26].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[26].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[27] = new FuzzyRule();

            reglas[27].addAntecedent(new FuzzyValue(preg[0], "alto")); 
            reglas[27].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[27].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[27].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[27].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[27].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[28] = new FuzzyRule();

            reglas[28].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[28].addAntecedent(new FuzzyValue(preg[1], "alto")); 
            reglas[28].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[28].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[28].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[28].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[29] = new FuzzyRule();

            reglas[29].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[29].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[29].addAntecedent(new FuzzyValue(preg[2], "alto")); 
            reglas[29].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[29].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[29].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[30] = new FuzzyRule();

            reglas[30].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[30].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[30].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[30].addAntecedent(new FuzzyValue(preg[3], "alto")); 
            reglas[30].addAntecedent(new FuzzyValue(preg[4], "nada")); 
            reglas[30].addConclusion(new FuzzyValue(resultado, "no")); 




            reglas[31] = new FuzzyRule();

            reglas[31].addAntecedent(new FuzzyValue(preg[0], "nada")); 
            reglas[31].addAntecedent(new FuzzyValue(preg[1], "nada")); 
            reglas[31].addAntecedent(new FuzzyValue(preg[2], "nada")); 
            reglas[31].addAntecedent(new FuzzyValue(preg[3], "nada")); 
            reglas[31].addAntecedent(new FuzzyValue(preg[4], "alto")); 
            reglas[31].addConclusion(new FuzzyValue(resultado, "no")); 


        }
        catch(FuzzyException e){
            System.out.println("error 'iniciar_reglas':");
            e.printStackTrace();
        }
    }


    /*****************
     * iniciar.
     *
     * inicializa las variable fuzzy para las 5 preguntas. Y agrega los términos
     * para cada variable.
     *
     *****************/
    public void iniciarVariables(){

        RightLinearFunction rlf = new RightLinearFunction();
        LeftLinearFunction llf = new LeftLinearFunction();

        try{

            /****
             * 1º pregunta
             * Sobre el grado o cantidad de palpitaciones que se sufre
             ****/
            preg[0] = new FuzzyVariable("palpitaciones", 0.0, 5.0, "grado");
            
            // se agrega una condicion (term) para cuando no se tiene palpitaciones
            // se utiliza la parte derecha de un trapezoide. Valores: 1/1.5, 0/4.5 
            preg[0].addTerm("nada", new RFuzzySet(1.5, 4.0, rlf));
            
            // se agrega una condicion cuando se tiene un alto grado de palpitaciones
            // se usa la parte izquierda de un trapezoide. Valores: 0/1.5, 1/4.5
            preg[0].addTerm("alto", new LFuzzySet(1.5, 4.5, llf));


            /****
             * 2º pregunta
             * Sobre la sensacion de ahogo o falta de aire
             ****/
            preg[1] = new FuzzyVariable("falta-aire", 0.0, 5.0, "grado");
            
            // se agrega un condicion cuando no se tiene la sensacion de ahogo 
            // se crea la parte derecha de un trapezoide
            preg[1].addTerm("nada", new RFuzzySet(1.5, 4.0, rlf));
            
            // condicion cuando la sensacion es alta
            // se usa la parte izquierda de un trapezoide
            preg[1].addTerm("alto", new LFuzzySet(1.5, 4.5, llf ));

            
            /****
             * 3º pregunta
             * Sobre la sensacion de opresion en la parte torácica.
             ****/
            preg[2] = new FuzzyVariable("molestar-toracico", 0.0, 5.0, "grado");
            
            // cuando no esta la sensacion de opresion torácica
            // se crea la parte derecha de un trapezoide
            preg[2].addTerm("nada", new RFuzzySet(1.5, 4.0, rlf ));
            
            // cuando hay opresion o malestar torácico
            // se usa la parte izquierda de un trapezoide
            preg[2].addTerm("alto", new LFuzzySet(1.5, 4.5, llf ));


            /****
             * 4º pregunta
             * Sobre la sensacion de miedo a perder el control o volverse loco.
             ****/
            preg[3] = new FuzzyVariable("miedo-locura", 0.0, 5.0, "grado");
            
            // cuando no existe tal miedo
            // se crea la parte derecha de un trapezoide
            preg[3].addTerm("nada", new RFuzzySet(1.5, 4.0, rlf ));

            // cuando el miedo es alto
            // se usa la parte izquierda de un trapezoide
            preg[3].addTerm("alto", new LFuzzySet(1.5, 4.5, llf ));



            /****
             * 5º pregunta
             * Sobre la sensacion de escalofrios o sofocaciones (calor)
             ****/
            preg[4] = new FuzzyVariable("escalofrios", 0.0, 5.0, "grado");
            
            // no hay tales sensaciones
            // se crea la parte derecha de un trapezoide
            preg[4].addTerm("nada", new RFuzzySet(1.5, 4.0, rlf ));
            
            // sensacion alta de sofocacion o escalofrios
            // se usa la parte izquierda de un trapezoide
            preg[4].addTerm("alto", new LFuzzySet(1.5, 4.5, llf ));

            /*************
             * Resultado
             *************/
            resultado = new FuzzyVariable("crisis-panico", 0.0 , 5.0, "grado");
            // se agrega un termino para cuando no hay crisis de panico
            // se usa la parte derecha de un trapezoide
            resultado.addTerm("no", new RFuzzySet(0.0, 1.5, rlf ));
            // Cuando hay una posible crisis de panico
            // Se utiliza un trapezoide
            resultado.addTerm("moderado", new TrapezoidFuzzySet(1.0, 2.0, 3.0, 4));
            // Cuando la posibilidad de crisis de panico es alta
            // Se utiliza la parte izquierda de un trapezoide
            resultado.addTerm("alto", new LFuzzySet(3.0, 4.0, llf ));

        }
        catch( FuzzyException e ){

            System.out.println("error 'iniciar'\n");
            e.printStackTrace();

        }
    }
}
